.PHONY: clean-pyc clean-build clean

help:
	@echo "clean       - remove all build, test, coverage and Python runtimes (may need sudo)"
	@echo "clean-build - remove build artifacts (may need sudo)"
	@echo "clean-pyc   - remove Python runtime artifacts"
	@echo "clean-test  - remove test and coverage artifacts"
	@echo "test        - run tests quickly with the default Python"
	@echo "release     - package and upload a release"
	@echo "dist        - package"
	@echo "install     - install the package to the active Python's site-packages"
	@echo "stats       - count files and lines of .py files"
	@echo "upload      - upload package to devpi repo"

clean: clean-build clean-pyc clean-test

clean-build:
	rm -fr build/
	rm -fr dist/
	find . -name '*.egg-info' -exec rm -fr {} +
	find . -name '*.egg' -exec rm -fr {} +

clean-pyc:
	find . -name '*.pyc' -delete
	find . -name '__pycache__' -exec rm -fr {} +

clean-test:
	rm -fr .cache/

requirements: ## Install requirements
	pip install -r requirements.txt
	pip install -r tests/requirements.txt

test:
	python setup.py test

check:
	find . -type f -name "*.py" -exec grep -l "print" {} +
	find . -type f -name "*.py" -exec grep -l "set_trace()" {} +

release: clean
	python setup.py sdist upload
	python setup.py bdist_wheel upload

dist: clean
	python setup.py sdist
	python setup.py bdist_wheel
	ls -l dist

install: clean
	python setup.py install

stats:
	@echo `find . -name '*.py' | wc -l` 'files'
	@echo `(find ./ -name '*.py' -print0 | xargs -0 cat ) | wc -l` 'lines'

upload:
	devpi login root --password=$(PASSWORD)
	devpi use root/stage
	devpi upload
