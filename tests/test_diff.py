# coding: utf-8

from ..pywalker import find_differences


def test_simple_symmetrical():
    files = list(find_differences(('tests/names/dir1', 'tests/names/dir2'), (), (), 2, False, False, False, 0))
    assert len(files) == 2


def test_simple_no_symmetrical():
    files = list(find_differences(('tests/names/dir1', 'tests/names/dir2'), (), (), 0, False, False, False, 0))
    assert len(files) == 1


def test_subdir():
    files = list(find_differences(('tests/names/dir1', 'tests/names/dir3'), (), (), 2, False, False, False, 2))
    assert len(files) == 1


def test_subdir_full_name():
    files = list(find_differences(('tests/names/dir1', 'tests/names/dir3'), (), (), 2, True, False, False, 2))
    assert len(files) == 3


def test_contents():
    files = list(find_differences(('tests/contents', 'tests/contents/subdir'), (), (), 1, False, False, False, 0))
    assert len(files) == 0
    files = list(find_differences(('tests/contents', 'tests/contents/subdir'), (), (), 1, False, False, True, 0))
    assert len(files) == 1
