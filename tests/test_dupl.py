# coding: utf-8

from ..pywalker import find_duplicates


def test_simple_recursion_1():
    files = list(find_duplicates(['tests/names'], (), (), False, False, 1))
    assert len(files) == 1
    assert len(files[0][1]) == 2


def test_simple_recursion_2():
    files = list(find_duplicates(['tests/names'], (), (), False, False, 2))
    assert len(files) == 1
    assert len(files[0][1]) == 3


def test_simple_no_recursion():
    files = list(find_duplicates(['tests/names'], (), (), False, False, 0))
    assert len(files) == 0


def test_length():
    files = list(find_duplicates(['tests/contents'], (), (), True, False, 0))
    assert len(files) == 1
    assert len(files[0][1]) == 3


def test_content():
    files = list(find_duplicates(['tests/contents'], (), (), False, True, 0))
    assert len(files) == 1
    assert len(files[0][1]) == 2
