# PYWALKER

A command line utility that searches efficiently for files differences or similarities
in a given set of folders.
Two different commands are provided:
 - dupl(icates): find duplicate files on a set of directories (1 or more)
 - diff(erences): find different files between 2 directories (symmetrical or not)
 
Search will recurse in subdirectories with controlled depth (0 no recurse, default=9), optional
forbiden directories and optional files extensions.

For duplicate, primary criterion is based on file name by default but you can use length or content as well.
Finding duplicates based on content on my home Wifi low perf NAS with about 15k photos took about 15 seconds.
 It is efficient because files with same length (though potentially same contents) are not frequent.

For difference, primary criterion can be on just name or on full relative path name (find files with same name but
different locations). Additionally, length or content can be used as a secondary difference criterion.

The philosophy behind duplicate search is that most files are different, so the cost of comparing files
by length/content is very low.
On the contrary, in the case of difference search, you compare very similar directories having
many identical files. In this case the cost of length/content compare is high and it is not recommended
to use it unless you realy need it and are ready to wait some time for a result.

An option now allows to interactively remove duplicate files.

### Installation:

Clone this project, then cd inside it.

Run `sudo clingon pywalker.py`

You can run it now: `pywalker -?`

You may need to install its dependencies (at system level):

`sudo pip install clingon inquirer`


# Use Cases

### Duplicates

Find duplicate files (based on file names) in a single directory, with verbose output:

`pywalker dupl {DIR}`

Find duplicate files (based on file contents) in a single directory with interactive delete:

`pywalker dupl {DIR} -c -d`

Find duplicate files in 2 directories with automatic delete proposed to you:

`pywalker dupl {DIR1} {DIR2} -d -r 0`

### Differences

Find different files (based on file names) in 2 directories, with verbose output:

`pywalker diff {DIR1} {DIR2}`

Find different files (based on file names) in 2 directories, but only from first directory point of view
and pipe result to another command

`pywalker diff {DIR1} {DIR2} -s 0 -p | command`
