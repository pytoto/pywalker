#! /usr/bin/env python

from collections import defaultdict, deque
from itertools import chain
import os

from clingon import clingon
import inquirer


class ParameterError(Exception):
    pass


# HELPER FUNCTIONS AND CLASSES

def memoize(f):
    """ Memoization decorator for a function with positional args only
    """
    class memodict(dict):
        def __getitem__(self, *key):
            return dict.__getitem__(self, key)
        def __missing__(self, key):
            ret = self[key] = f(*key)
            return ret
    return memodict().__getitem__


def files_equal(file1, file2):
    """ Compare 2 files by content
    :return: True if files are equal
    """
    with open(file1, 'rb') as f1, open(file2, 'rb') as f2:
        while True:
            data1 = f1.read(4096)
            data2 = f2.read(4096)
            if data1 != data2:
                return False
            if not (data1 and data2):
                return True


def group_objects(obj_iter, comp):
    """ Compare objects produced by an iterable and returns a list of list of objects.
    In each inner list, objects are equal wrt comp function.
    :param obj_iter: an iterable of objects
    :param comp: a comparator function with 2 parameters
    :return: a list of list of objects
    """
    it = iter(obj_iter)
    result = [[next(it)]]
    for o in it:
        for group in result:
            if comp(o, group[0]):
                group.append(o)
                break
        else:
            result.append([o])
    return result


class IterateFiles(object):
    """ Iterate files in a directory with controlled recursion depth, optional forbidden subdirs
        and optional list of allowed files extensions (only simple extensions allowed, ie .gz not .tar.gz)
    """
    def __init__(self, directory, except_dirs, allowed_extensions, depth):
        self.directory, self.except_dirs, self.depth = directory, except_dirs, depth
        self.allowed_extensions = [(a if a.startswith('.') else '.' + a) for a in allowed_extensions]

    def __iter__(self):
        return self.iterate_files(self.directory, self.depth)

    def iterate_files(self, directory, depth):
        if depth < 0:
            return
        for entry in os.scandir(directory):
            if entry.is_file():
                if (not self.allowed_extensions or
                        os.path.splitext(entry.name)[-1] in self.allowed_extensions):
                    yield entry
            elif entry.is_dir() and entry.name not in self.except_dirs:
                # will upgrade to 'yield from' soon
                for x in self.iterate_files(entry.path, depth-1):
                    yield x


class UndoIterator(object):
    """ wraps a plain iterator to offer a two-way iterator
        with a controlled back step depth
    """
    def __init__(self, iterator, depth=2, start_index=0):
        self._iterator = iterator
        self._queue = deque(maxlen=depth+1) if depth > 0 else []
        self._back_queue = []
        self.index = start_index - 1  # simulate enumerate() on the go

    def __iter__(self):
        return self

    def __len__(self):
        return len(self._queue)

    def next(self):
        elt = self._back_queue.pop() if self._back_queue else next(self._iterator)
        self._queue.append(elt)
        self.index += 1
        return elt

    def prev(self):
        elt = self._queue.pop()
        self._back_queue.append(elt)
        self.index -= 1
        return elt

    def cur(self):
        return self._queue[-1]


# FUNCTIONS FINDING DUPLICATES AND DIFFERENCES

def find_duplicates(directories, except_dirs, allowed_extensions, use_length, use_content, recurse_depth):
    """ Duplicates are files with same name, same length or same content
    :param directories: a list of directories (at least one)
    :param except_dirs: list of skipped subdirectories
    :param allowed_extensions: list of allowed extensions
    :param use_length: use length of file as primary difference criterion
    :param use_content: use length then content of file as primary difference criterion
    :param recurse_depth: depth of search in subdirectories (0 no recurse)
    :return: a list of pairs (file id [name, length or id], list of file paths)
    """
    if len(directories) < 1:
        raise ParameterError("You must specify at least one path")
    if use_length and use_content:
        raise ParameterError("length and content criteria are mutually exclusive")
    use_length = use_length or use_content
    files_dict = defaultdict(list)
    for d in directories:
        for f in IterateFiles(d, except_dirs, allowed_extensions, recurse_depth):
            files_dict[str(f.stat().st_size) if use_length else f.name].append(f.path)
    if use_content:
        def iterate_files_content(files_dict):
            # workaround generators limitation found in Python<3.5
            for k, files in files_dict.items():
                if len(files) > 1:
                    for i, group in enumerate(group_objects(files, files_equal)):
                        if len(group) > 1:
                            yield "%s_%d" % (k, i), group
        return iterate_files_content(files_dict)
    return ((k, files) for k, files in files_dict.items() if len(files) > 1)


def find_differences(directories, except_dirs, allowed_extensions, sides, full_name,
                     use_length, use_content, recurse_depth):
    """ differences are files that exist in one directory but not in the other
    :param directories: 2 directories
    :param except_dirs: skipped subdirectories
    :param allowed_extensions: list of allowed extensions
    :param sides: if 0, only files found in first directory but not in second are reported,
           if 1, only files found in second directory but not in first,
           if 2, files found on both sides are reported.
    :param full_name: use relative path as difference criterion
    :param use_length: use length of file as secondary difference criterion
    :param use_content: use content of file as secondary difference criterion
           (use with care as potentially many files may be compared block by block)
    :param recurse_depth: depth of recursion in subdirectories (0 no recurse)
    :return: a generator of pairs (file names / relative paths, path) (depends on option 'full_name')
    """
    if not 0 <= sides <= 2:
        raise ParameterError("Option 'sides' must be 0 (first dir), 1 (second dir) or 2 (both)")
    l = len(directories)
    if l != 2:
        raise ParameterError("You must specify 2 paths (%d specified)" % l)
    files_dict = [{}, {}]
    for i, d in enumerate(directories):
        fd = files_dict[i]
        path_len = len(d) + 1
        for f in IterateFiles(d, except_dirs, allowed_extensions, recurse_depth):
            fd[f.path[path_len:] if full_name else f.name] = f
    if use_length:
        def diff(f0, f1):
            return f0.stat().st_size != f1.stat().st_size
    elif use_content:
        @memoize
        def diff(f0, f1):
            if f0.stat().st_size == f1.stat().st_size:
                return not files_equal(f0.path, f1.path)
    else:
        def diff(f0, f1):
            return False
    gene = (((k, v.path) for k, v in files_dict[0].items() if (k not in files_dict[1] or diff(v, files_dict[1][k]))),
            ((k, v.path) for k, v in files_dict[1].items() if (k not in files_dict[0] or diff(files_dict[0][k], v))))
    if sides == 2:
        return chain(*gene)
    return gene[sides]


# OUTPUT AND DELETE FUNCTIONS

def print_results(files, res_type, verbose):
    if verbose:
        print('')
        i = -1
        for i, f in enumerate(files):
            if res_type == 'duplicate':
                it = iter(f[1])
                p = next(it)
                print("%s: %s" % (f[0], p))
                bl = ' ' * len(f[0])
                for p in it:
                    print("%s: %s" % (bl, p))
            else:
                print("%s: %s" % f)
        print("Found %d %s%s" % (i+1, res_type, 's' if i > 0 else ''))
    else:
        for f in files:
            print(';'.join(f[1])) if res_type == 'duplicate' else print(f[1])


def delete_duplicates(files, directories, recurse_depth):
    """ delete duplicate files automatically only in the safe case of:
        - 2 directories
        - no recurse depth
        otherwise goes to fully interactive delete
    """
    second_dir = directories[1]
    if len(directories) == 2 and not recurse_depth and \
       input("Automatically removed duplicates in %s, proceed ? (y/n) " % second_dir) == 'y':
        non_interactive_delete(files, second_dir)
    else:
        interactive_delete(files)


def non_interactive_delete(files, directory):
    files2remove = [f[1][1] for f in files if directory in f[1][1]]
    delete_files(files2remove)


def interactive_delete(files):
    """ interactive delete shows a select list for each duplicate case (can be tri- or multi-plicate)
        the 2 (3, 4, ...) first elements are duplicates, then
        - skip to skip this case
        - quit to quit interactive delete with no action
        - proceed to quit interactive delete and remove files already enlisted for deletion
        - back to go back to previous case
        WARNING: the selected file is the one that is kept, all other files in this case are
        enlisted for deletion
    """
    print('')
    recorded_actions = []
    actions1 = ['skip', 'quit']
    actions2 = ['skip', 'back', 'proceed', 'quit']
    print("Entering interactive delete mode")
    ui = UndoIterator(files, depth=0)  # infinite depth
    for file, paths in ui:
        choices = [p.decode('utf8') for p in paths] + (actions2 if len(ui) > 1 else actions1)
        question = [
            inquirer.List('choice',
                          message=" [%d] file %s, which path to KEEP, or action ?" % (ui.index, file),
                          choices=choices
                          )
        ]
        answer = inquirer.prompt(question).values()[0]
        if answer == 'quit':
            if input("About to quit, confirm ? (y/n) ") == 'y':
                raise RuntimeError("QUIT")
            ui.prev()
        elif answer == 'back':
            recorded_actions.pop()
            ui.prev()
            ui.prev()
        elif answer == 'proceed':
            recorded_actions.append('skip')
            break
        else:
            recorded_actions.append(answer)
    files2remove = []
    for r, q in zip(recorded_actions, ui._queue):
        if r != 'skip':
            files2remove.extend(set(q[1]) - {r.encode('utf8')})
    delete_files(files2remove)


def delete_files(files):
    l = len(files)
    if not l:
        print("Nothing to do, bye")
    else:
        extract = files[:3]
        print("About to remove %d files (%s%s) " % (l, ' '.join(extract), '...' if len(extract) < l else ''))
        if input("Confirm ? (y/n) ") == 'y':
            for f in files:
                print("Deleting %s" % f)
                os.remove(f)
        else:
            raise RuntimeError("QUIT")


if __name__ == '__main__':
    @clingon.clize(use_length=('l',), use_content=('c',))
    def runner(command, use_length=False, use_content=False, full_name=False, dupl_delete=False,
               except_dirs=[], allowed_extensions=[], recurse_depth=9, sides=2, pipe=False, *directories):
        """
        *******************************
        *           PYWALKER          *
        *******************************
        Command line utility that searches efficiently for files differences or similarities in a given set of folders.
        Two different commands are provided:
         - dupl(icates): find duplicate files on a set of directories (1 or more)
         - diff(erences): find different files between 2 directories (symmetrical or not)
        Search will recurse in subdirectories with controlled depth (0 no recurse, default=9).
        For duplicates, criterion is based on file name by default, but you can specify length or content as well.
        For differences, criterion is based on file name by default but can be on full relative path name (find files with same name but different locations).
        Additionally, length or content can be used as a secondary difference criterion.
        """
        allowed_commands = (('dupl', 'duplicate'), ('diff', 'difference'))
        root_command = command[:4].lower()
        verbose = not pipe
        try:
            if root_command == allowed_commands[0][0]:  # DUPLICATE
                if full_name or sides != 2:
                    raise clingon.RunnerError("Option 'full-name' or 'sides' is not allowed for this command")
                files = find_duplicates(directories, except_dirs, allowed_extensions, use_length, use_content,
                                        recurse_depth)
                if dupl_delete:
                    delete_duplicates(files, directories, recurse_depth)
                else:
                    print_results(files, allowed_commands[0][1], verbose)
                return
            if root_command == allowed_commands[1][0]:  # DIFFERENCE
                if dupl_delete:
                    raise clingon.RunnerError("Option 'dupl-delete' is not allowed for this command")
                files = find_differences(directories, except_dirs, allowed_extensions, sides, full_name,
                                         use_length, use_content, recurse_depth)
                print_results(files, allowed_commands[1][1], verbose)
                return
        except ParameterError as e:
            raise clingon.RunnerError(e.args[0])
        raise clingon.RunnerErrorWithUsage("Command must be one of %s, you said '%s'" % (allowed_commands, command))
